let userNum = Number(prompt("Enter a number:"));

function checkIfDivisible(userNum) {
    console.log("The number you provided is " + userNum);
    for (let x = userNum; x >= 0; x--) {
        if (x <= 50) {
            console.log("The current value is less than or equal to 50" + ". Terminating the loop.")
            break;
        }
        else if (x % 10 === 0) {
            console.log("The number is divisible by 10. Skipping the number.");
            continue;
        }
        else if (x % 5 === 0) {
            console.log(x);
            continue;
        }
    };
}
checkIfDivisible(userNum);

let word = "supercalifragilisticexpialidocious";
let consonants = "";

function checkIfConsonant(word) {
    for (i = 0; i < word.length; i++) {
        if( word[i] === 'a' || 
            word[i] === 'e' || 
            word[i] === 'i' || 
            word[i] === 'o' || 
            word[i] === 'u' 
        ) 
        {
            continue;
        } 
        else 
        {
            consonants = consonants.concat(word[i]);
        }
    }
    console.log(word);
    console.log(consonants);
}
checkIfConsonant(word);
