// mini-activity

/* function isUser(userID){
    let userName;
    switch (userID) {
        case 1:
            userName = 'Naruto';
            break;
        case 2:
            userName = 'Luffy';
            break;
        case 3:
            userName = 'Itadori';
            break;  
        case 4:
            userName = 'Midoriya';
            break;
        case 5:
            userName = 'Light Yagami';
            break;    
        default:
            console.log("Please input a valid user.");
            break;
    }
    return userName;
} */


let count = 5;

// WHILE LOOP
/* 
    - takes in an expression/condition
    - expressions are any unit of code that can be evaluated to a value
    - if the evaluation of the condition is true, the statement will be executed
    - a loop will iterate certain number of times until the condition is/isn't met
    - "iteration" is the term given to the repetition

    SYNTAX:
        while (condition) {
            statement/s;
        }
*/
while (count !== 0) {
    console.log("While loop result: " + count);
    count--;
}

// 2nd mini-activity
count = 0;
// checks for the value of count
while (count <= 10) {
    //statement to be performed if the condition remains true
    console.log(count);
    //increments the value of count, also serves as loop breaker
    count++;
}

// DO-WHILE LOOP
/* 
    - works like the while loop. But unlike while loop, do-while loops guarantee that the code will be executed atleast once

    SYNTAX:
        do{
            statement/s
        } while (condition)
*/
/*  
    - Number function works similarly to the "parseInt" function 
    - both differ significantly in terms of the process they undertake in converting information into a number data type.
    - how do-while loop works (in reference to code below):
        1. the statements in "do" block executes once
        2. the message in the statements will be executed
        3. after executing once, the while statement will evaluate whether to run the iteration of the loop or not based on the expression(if the number is less than 10)
        4. if the condition is ture, an iteration will be conducted
        5. if the condition is false, the loop will stop
*/
/* let number = Number(prompt("Give me a number"));;
do {
    console.log("Do-while Loop: " + number);
    number++;
} while (number < 10) */


// FOR LOOP
/* 
    - most flexible looping compared to other loop functions
    - has 3 parts
        1. initialization - tracks the progression of the loop
        2. condition/expression - will be evaluated which will determine whether the loop will run one more time
        3. final expression - indicates how to advance the loop
    
    SYNTAX:

    for (i = 0; condition; i++) {
        statement/s
    }
*/
/* 
    Stages of For Loop
    - initialize the variable "count" that has the value of 0;
    - the condition/expression that is to be assessed is if the value of "count" is less than or equal to 20
    - perform the statements should the condition returns true
    - increment the value of count
*/
for (let count = 0; count <= 20; count++) {
    console.log("For Loop: " + count);
};

for (let count = 5; count > 5; count--) {
    console.log("For Loop: " + count);
};

// using strings
/* 
    characters in a string may be counted using the .length property; the property measures the number of elements (characters), not the string itself
*/
let myString = "alex";
console.log(myString);

for (let x = 0; x < myString.length; x++) {
    console.log(myString[x]);
};

let myName = "JAYCEE";
for (let x = 0; x < myName.length; x++) {
    if (myName[x].toLowerCase() === 'a' || myName[x].toLowerCase() === 'e' || myName[x].toLowerCase() === 'i' || myName[x].toLowerCase() === 'o' || myName[x].toLowerCase() === 'u') 
    {
        console.log(3);
    } else {
        console.log(myName[x].toLowerCase());
    }
};


// Continue and Break Statements

for (let count = 0; count <= 20; count++) {
    if (count % 2 === 0) {
        //tells the code to continue to the next iteration of the loop; it will ignore all preceding of the block
        continue;
    }
    // if the remainder is not equal to zero, this will be executed
    console.log("Continue and Break: " + count);

    if (count > 10) {
        // tells the code to terminate/stop loop even if the expression/condition of loop will return true for the next iteration
        break;
    };
    
};

// mini-activity
let newName = "alexandro";

for (x = 0; x < newName.length; x++) {
    if (newName[x] === 'a') {
        continue;
    }
    if (newName[x] === 'd') {
        break;
    }
    console.log(newName[x]);
}






